require 'fox16'

include Fox


class ButtonWindow < FXMainWindow
  def initialize(app)
    super(app, "Task 2", :opts => DECOR_ALL, :x => 100, :y => 100)

    # Create a frame to place a button
    contents = FXHorizontalFrame.new(self,
      LAYOUT_SIDE_LEFT|FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y|PACK_UNIFORM_WIDTH,
      :padding => 20)

    # Create a button and place it on the frame
    @button = FXButton.new(
      contents,
      "&Click to change me",
      :opts => FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X|LAYOUT_CENTER_Y|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,
      :width => 200,
      :height => 50
    )

    # Connect button to it's `onclick` event
    @button.connect(SEL_COMMAND, method(:onCmdShowDialogModal))

    # Cerate a fontDialog window to show it in the future.
    @fontDialog = FXFontDialog.new(self, "Font dialog")
  end

  def onCmdShowDialogModal(sender, sel, ptr)
    # Show font dialog to ask user to select font
    @fontDialog.execute

    # Once fontFialog was closed, we got to this part of the code

    # Create font fron font description that user has selected
    font = FXFont.new(self.getApp(), @fontDialog.getFontSelection)
    font.create

    # Apply new font to button
    @button.setFont(font)

    # As we're using modal window, we need to return 1 or 0 on button click
    # event, so the main window could be focused
    return 1
  end

  def create
    super
    show(PLACEMENT_SCREEN)
  end
end


if __FILE__ == $0
  # Construct an application
  application = FXApp.new("Task 2")

  # Construct the main window
  ButtonWindow.new(application)

  # Create the application
  application.create

  # Run it
  application.run
end
